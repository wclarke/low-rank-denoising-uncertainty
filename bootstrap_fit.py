import argparse
import numpy as np
import nibabel as nib
import subprocess
from pathlib import Path
import shutil


def gen_fit_call(data, mask, output_loc, config, wref=None):
    if wref:
        return ['fsl_mrsi',
                '--data', data,
                '--mask', mask,
                '--output', output_loc,
                '--h2o', str(wref),
                '--single_proc',
                '--config', config]
    else:
        return ['fsl_mrsi',
                '--data', data,
                '--mask', mask,
                '--output', output_loc,
                '--single_proc',
                '--config', config]


def gen_heuristic_cov(var, cmplxcov):
    large_cov = np.block([[cmplxcov.real, cmplxcov.imag], [cmplxcov.imag.T, cmplxcov.real]])
    large_cov -= np.diag(np.diag(large_cov))
    # Inputs have been scaled by the variance of the noisy data.
    # Once split into the real and imaginary components then the scaling
    # should be halved.
    large_cov += np.diag(np.concatenate((var, var))) / 2
    # large_cov /= 2
    return large_cov


def make_bs_instantiation(data, var, mask, covar):
    peturbed_data = np.zeros_like(data)
    shape = data.shape[3]
    rng = np.random.default_rng()
    for idx in np.ndindex(data.shape[:3]):
        if mask[idx[0], idx[1], idx[2]]:
            covar_mat = gen_heuristic_cov(var[idx[0], idx[1], idx[2], :], covar)
            peturbed = rng.multivariate_normal(np.concatenate((data[idx[0], idx[1], idx[2], :].real,
                                                               data[idx[0], idx[1], idx[2], :].imag)),
                                               covar_mat,
                                               check_valid='ignore')
            peturbed_data[idx[0], idx[1], idx[2], :] = peturbed[:shape] + 1j * peturbed[shape:]
    return peturbed_data


def main(raw_args=None):
    parser = argparse.ArgumentParser()
    parser.add_argument('data', help="Input data to fit", type=Path)
    parser.add_argument('outloc', help="Output_location", type=Path)
    parser.add_argument('mask', help="Spatial mask", type=Path)
    parser.add_argument('--fit_config', help="Fit configuration .txt file", type=str)
    parser.add_argument('--reps', help="Number of bootstrapping fits", type=int, default=10)
    parser.add_argument('--ref', help="Water reference data", type=Path, default=None)
    args = parser.parse_args(raw_args)

    # 1 - Deal with input
    # Load input
    original_data = nib.load(args.data)

    mask = nib.load(args.mask).get_fdata()
    mask = np.atleast_3d(mask)
    # Find variance and covariance files and load
    basestr = args.data.with_suffix('').with_suffix('').name
    var_path = args.data.parent / (basestr + '_var.nii.gz')
    var_data = nib.load(var_path)

    covar_path = args.data.parent / (basestr + '_covar')
    covar = np.loadtxt(covar_path, dtype=complex)

    # Make output directory
    args.outloc.mkdir(exist_ok=True)

    # 2 - In BS loop
    for idx in range(args.reps):
        # A - Generate peturbed data
        # peturb and save to fit directory
        peturbed_data = make_bs_instantiation(original_data.get_fdata(dtype=complex),
                                              np.real(var_data.get_fdata(dtype=complex)),
                                              mask,
                                              covar)

        peturbed_img = nib.Nifti2Image(peturbed_data, original_data.affine, original_data.header)
        tmp_file = args.outloc / 'curr_data.nii.gz'
        nib.save(peturbed_img, tmp_file)

        # B - Fit peturbed data
        # Output to own dir
        bs_out = args.outloc / f'bs_{idx:03.0f}'
        fit_call = gen_fit_call(str(tmp_file), str(args.mask), str(bs_out), args.fit_config, wref=args.ref)
        subprocess.check_call(fit_call)

        # C cleanup
        # Delete the tmporary data.
        tmp_file.unlink()

    # 3 - Accumulate results
    # Concs are mean of all concentration data
    # fit CRLB are mean of all CRLB
    # BS STD is std of the concentration data
    # loop through all the nii.gz files in the concs (raw and molarity) and uncertainties

    # fit uncertainties first
    out_uncertainties = args.outloc / 'fit_uncertainties'
    out_uncertainties.mkdir(exist_ok=True)
    for c_file in args.outloc.rglob('bs_000/uncertainties/*.nii.gz'):
        accum_data = []
        for idx in range(args.reps):
            file = args.outloc / f'bs_{idx:03.0f}' / 'uncertainties' / c_file.name
            c_data = nib.load(file)
            accum_data.append(c_data.get_fdata())
        accum_data = np.asarray(accum_data)

        accum_img = nib.Nifti1Image(accum_data.mean(axis=0), c_data.affine, c_data.header)
        nib.save(accum_img, out_uncertainties / c_file.name)

    # MC uncertainties (raw)
    out_concs = args.outloc / 'uncertainties' / 'raw'
    out_concs.mkdir(exist_ok=True, parents=True)
    for c_file in args.outloc.rglob('bs_000/concs/raw/*.nii.gz'):
        accum_data = []
        for idx in range(args.reps):
            file = args.outloc / f'bs_{idx:03.0f}' / 'concs' / 'raw' / c_file.name
            c_data = nib.load(file)
            accum_data.append(c_data.get_fdata())
        accum_data = np.asarray(accum_data)

        accum_img = nib.Nifti1Image(accum_data.std(axis=0), c_data.affine, c_data.header)
        nib.save(accum_img, out_concs / c_file.name)

    # MC uncertainties (molarity)
    out_concs = args.outloc / 'uncertainties' / 'molarity'
    out_concs.mkdir(exist_ok=True, parents=True)
    for c_file in args.outloc.rglob('bs_000/concs/molarity/*.nii.gz'):
        accum_data = []
        for idx in range(args.reps):
            file = args.outloc / f'bs_{idx:03.0f}' / 'concs' / 'molarity' / c_file.name
            c_data = nib.load(file)
            accum_data.append(c_data.get_fdata())
        accum_data = np.asarray(accum_data)

        accum_img = nib.Nifti1Image(accum_data.std(axis=0), c_data.affine, c_data.header)
        nib.save(accum_img, out_concs / c_file.name)

    # Concs (raw)
    out_concs = args.outloc / 'concs' / 'raw'
    out_concs.mkdir(exist_ok=True, parents=True)
    for c_file in args.outloc.rglob('bs_000/concs/raw/*.nii.gz'):
        accum_data = []
        for idx in range(args.reps):
            file = args.outloc / f'bs_{idx:03.0f}' / 'concs' / 'raw' / c_file.name
            c_data = nib.load(file)
            accum_data.append(c_data.get_fdata())
        accum_data = np.asarray(accum_data)

        accum_img = nib.Nifti1Image(accum_data.mean(axis=0), c_data.affine, c_data.header)
        nib.save(accum_img, out_concs / c_file.name)

    # Concs (molarity)
    out_concs = args.outloc / 'concs' / 'molarity'
    out_concs.mkdir(exist_ok=True, parents=True)
    for c_file in args.outloc.rglob('bs_000/concs/molarity/*.nii.gz'):
        accum_data = []
        for idx in range(args.reps):
            file = args.outloc / f'bs_{idx:03.0f}' / 'concs' / 'molarity' / c_file.name
            c_data = nib.load(file)
            accum_data.append(c_data.get_fdata())
        accum_data = np.asarray(accum_data)

        accum_img = nib.Nifti1Image(accum_data.mean(axis=0), c_data.affine, c_data.header)
        nib.save(accum_img, out_concs / c_file.name)

    # 4 - Cleanup all other data
    for idx in range(args.reps):
        shutil.rmtree(args.outloc / f'bs_{idx:03.0f}')


if __name__ == '__main__':
    main()
