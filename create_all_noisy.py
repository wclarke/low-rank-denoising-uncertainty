"""Script to generate all the noisy data needed from the average fit.
Uses gen_noisy_reps script and most recent average fit data.

Generates 50x repetitions of:
- Noisy data generated at 20x noise level
- Noisy data generated at 10x noise level
- Noisy data generated at 5x noise level
- Noisy data generated at 1x noise level
- Noisy data generated at 1/2x noise level
"""

from gen_noisy_reps import main
from pathlib import Path
import shutil

noise_levels = [20, 10, 5, 1, 0.5]

# find most recent date string
avg_fit_store = Path('fit_for_synthetic')
date_strings = \
    [fp.stem.lstrip('synthetic_mask_').rstrip('.nii') for fp in avg_fit_store.glob('synthetic_mask_*.nii.gz')]
date_strings = sorted(date_strings)
str_to_use = date_strings[-1]

out_dir = Path('noisy_synthetic_data')
out_dir.mkdir(exist_ok=True)

# Copy mask and water ref
shutil.copy(avg_fit_store / f'synthetic_mask_{str_to_use}.nii.gz', out_dir / 'mask.nii.gz')
shutil.copy(avg_fit_store / f'wref_{str_to_use}.nii.gz', out_dir / 'wref.nii.gz')

for nl in noise_levels:

    main(['noisy_synthetic',
          str(avg_fit_store / f'noiseless_synthetic_{str_to_use}.nii.gz'),
          str(avg_fit_store / f'fitted_combined_{str_to_use}.pickle'),
          '-n', f'{nl}',
          '-r', '50',
          '-o', str(out_dir / f'noise_{nl}')])
