from fsl_mrs.utils import mrs_io
from fsl_mrs.utils.preproc import phasing, nifti_mrs_proc
from pathlib import Path
import nibabel as nib
import numpy as np

base_dir = Path('/Users/wclarke/Documents/Data/denoising_study/invivo')
all_input_data = []
for data in base_dir.rglob('hv10*/combined.nii.gz'):
    print(data)
    combined_nmrs = mrs_io.read_FID(str(data))
    
    # From nifti_mrs object generate fsl_mrs mrsi object
    tmp_mrsi = combined_nmrs.mrs()
    
    # Load and set mask
    mask = nib.load(str(data.parent / 'mask.nii.gz'))
    mask = np.asanyarray(mask.dataobj)
    if mask.ndim == 2:
        mask = np.expand_dims(mask, 2)
    tmp_mrsi.set_mask(mask)
    
    # Calculate phasing from masked region of the combined data.
    curr_data = tmp_mrsi.data[tmp_mrsi.mask]
    all_phase = []
    for fid in curr_data:
        _,ph,_ = phasing.phaseCorrect(fid, tmp_mrsi.header['bandwidth'],tmp_mrsi.header['centralFrequency']*1E6)
        all_phase.append(ph)
        
    add_phs = np.mean(all_phase) * 180/np.pi
    
    # Apply phase to all data (combined and single_)
    nifti_mrs_proc.apply_fixed_phase(combined_nmrs, add_phs).save(str(data))
    
    for sp in data.parent.glob('single*'):
        s_nmrs = mrs_io.read_FID(str(sp))
        nifti_mrs_proc.apply_fixed_phase(s_nmrs, add_phs).save(str(sp))