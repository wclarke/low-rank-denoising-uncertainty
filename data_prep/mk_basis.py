# Requires FSL-MRS fro HLSVD removal of TMS
basis_file = '3T_slaser_32vespa_1250.BASIS'

basis, names, bheader = mrs_io.read_basis(basis_file)


out_basis = Path('data/basis_set')
out_basis.mkdir(exist_ok=True)

for bb, nn, bh in zip(basis.T,names,bheader):
    plt.plot(FIDToSpec(bb).real)
    mod_basis = remove.hlsvd(bb,bh['dwelltime'],bh['centralFrequency'],(-0.02,0.02),limitUnits='ppm+shift')
    plt.plot(FIDToSpec(mod_basis).real,'--')
    plt.title(nn)
    plt.show()
    basis_dict = {'basis': {'basis_re': mod_basis.conj().real.tolist(),
                            'basis_im': mod_basis.conj().imag.tolist(),
                            'basis_centre': bh['centralFrequency']/1E6,
                            'basis_dwell': bh['dwelltime'],
                            'basis_width': 0.0,
                            'basis_name': nn}}
    with open(str(out_basis / f'{nn}.json'),'w') as fp:
        json.dump(basis_dict,fp)
