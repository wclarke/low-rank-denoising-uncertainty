import nibabel as nib
from pathlib import Path
import shutil
from fsl_mrs.utils import mrs_io
from subprocess import check_call

in_path = Path('/Volumes/WTCLarge/Data/asteel_1h_mrsi_raw')
out_path = Path('/Users/wclarke/Documents/Data/denoising_study/invivo')
# out_path.mkdir(exist_ok=True, parents=True)

# def load_and_save(path_in, path_out):
#     data = mrs_io.read_FID(str(path_in))
#     data.save(str(path_out))
    
# for fp in in_path.glob('hv10*'):
#     curr_out = out_path / fp.name
#     curr_out.mkdir(exist_ok=True)
    
#     shutil.copy(fp / '3_t1_mpr_ax_1mm_iso_withNose_32ch_v2.nii.gz', curr_out / 't1.nii.gz')
#     shutil.copy(fp / 'mask.nii.gz', curr_out / 'mask.nii.gz')
#     shutil.copy(fp / 'mask_to_fit.nii.gz', curr_out / 'mask_to_fit.nii.gz')
    
#     load_and_save(fp / 'wref.nii.gz', curr_out / 'wref.nii.gz')
#     load_and_save(fp / 'combined.nii.gz', curr_out / 'combined.nii.gz')
#     for sp in fp.glob('single_avg_*.nii.gz'):
#         load_and_save(sp, curr_out / sp.name)
        
        
for fp in out_path.rglob('t1.nii.gz'):
    print(fp)
    check_call(['fsl_deface', str(fp), str(fp), '-p', 'qc_deface'])
    