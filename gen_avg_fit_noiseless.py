'''Script to fit all the combined data, output into a pandas dataframe and generate noiseless synthetic MRSI data'''
from pathlib import Path
from datetime import datetime
import shutil

import nibabel as nib
import numpy as np
import pandas as pd

from fsl_mrs.utils import mrs_io, fitting, quantify
from fsl_mrs.utils.misc import parse_metab_groups
from fsl_mrs.utils.synthetic import synthetic_from_basis as synth
from fsl_mrs.core.nifti_mrs import gen_new_nifti_mrs

single_time_Str = datetime.now().strftime("%Y%m%d%H%M%S")

### Stage 1 - Fitting of high SNR in vivo data
base_dir = Path('data/in_vivo')
basis_file = 'data/basis_set'

output_folder = Path('fit_for_synthetic')
output_folder.mkdir(exist_ok=True)

# Load the data required
print('Loading the data')
all_input_data = []
for idx in range(0,5):
    data = base_dir / f'hv10{idx + 1}' / 'combined.nii.gz'
    tmp_nmrs = mrs_io.read_FID(str(data))
    tmp_nmrs_ref = mrs_io.read_FID(str(data.parent / 'wref.nii.gz'))

    # From nifti_mrs object generate fsl_mrs mrsi object
    # Include the reference and basis info
    tmp_mrsi = tmp_nmrs.mrs(basis_file=basis_file,
                            ref_data=tmp_nmrs_ref)

    # Load and set mask
    mask = nib.load(str(data.parent / 'mask.nii.gz'))
    mask = np.asanyarray(mask.dataobj)
    if mask.ndim == 2:
        mask = np.expand_dims(mask, 2)
    tmp_mrsi.set_mask(mask)

    # Remove inappropriate basis sets and set rescaling
    tmp_mrsi.ignore = ['HG', 'Ala', 'PE']
    tmp_mrsi.rescale = True
    all_input_data.append(tmp_mrsi)

    # Output data will mimic the orientation of first input.
    # save the affine for later write operation
    # Save the input mask and water reference in the output dimension
    if idx == 0:
        shutil.copy(data.parent / 'mask.nii.gz', output_folder / ('synthetic_mask_' + single_time_Str + '.nii.gz'))
        shutil.copy(data.parent / 'wref.nii.gz', output_folder / ('wref_' + single_time_Str + '.nii.gz'))
        output_affine = tmp_nmrs.voxToWorldMat
    print(f'Loaded hv10{idx + 1}')

print('Setup for fit')
# Fit the data, include FSL-MRS default MM peaks
def runvoxel(mrs_in, Fitargs):
    mrs, index, tissue_seg = mrs_in

    # Parse metabolite groups
    metab_groups = parse_metab_groups(mrs, 'combine_all')

    n = mrs.add_MM_peaks(gamma=40, sigma=30)
    new_metab_groups = [i + max(metab_groups) + 1 for i in range(n)]
    new_metab_groups = metab_groups + new_metab_groups

    res = fitting.fit_FSLModel(mrs, **Fitargs, metab_groups=new_metab_groups)

    # Quantification (internal and water referencing - no relaxation scaling)
    echo_time = 0.0
    repetition_time = 10.0
    q_info = quantify.QuantificationInfo(echo_time,
                                         repetition_time,
                                         mrs.names,
                                         mrs.centralFrequency / 1E6)

    res.calculateConcScaling(mrs,
                             quant_info=q_info,
                             internal_reference=['Cr', 'PCr'])

    return res, index

# Set fitting options - default FSL-MRS except for more flexible baseline
Fitargs = {'ppmlim': (0.2, 4.2),
           'method': 'Newton',
           'baseline_order': 3,
           'model': 'voigt'}

print('Doing fitting')
all_df = []
for sdx, mrsi in enumerate(all_input_data):
    print(f'Fitting {sdx}.')
    mrs = mrsi.mrs_from_average()
    Fitargs_init = Fitargs.copy()
    res_init, _ = runvoxel([mrs, 0, None], Fitargs_init)
    Fitargs['x0'] = res_init.params
    results = []
    for idx, mrs in enumerate(mrsi):
        res = runvoxel(mrs, Fitargs)
        tmp_res_df = res[0].fitResults
        # Noise STD estimated from final 300 FID points
        tmp_res_df['noise_sd'] = np.std(mrs[0].FID[300:])
        tmp_res_df['fid_scale'] = mrs[0].scaling['FID']
        tmp_res_df['voxel'] = idx
        results.append(tmp_res_df)
        if not idx % 20:
            print(f'{idx+1}/{mrsi.num_masked_voxels} voxels completed')
    res_df = pd.concat(results)
    res_df['subject'] = sdx
    all_df.append(res_df)

# Assemble dataframe from all fitted voxels and save it
df_final = pd.concat(all_df)
df_final = df_final.reset_index()
df_final = df_final.set_index(['subject', 'voxel'])
df_final.to_pickle(output_folder / ('fitted_combined_'+single_time_Str+'.pickle'))


### Stage 2 - Generate synthetic data and save out noiseless synthetic mrsi
# Synthetic data uses the median values from the five fitted datasets.
# Phase terms are removed and the baseline is zeroed.
print('Generating synthetic data.')
mrs = all_input_data[0].mrs_from_average()
mrs.add_MM_peaks()
names = mrs.names

all_syn_fids = []
for ii in df_final.mean(level=1).T:
    if not ii % 20:
        print(ii)
    curr_df = df_final.median(level=1).loc[ii]
    # Extract each parameter type
    concs = curr_df[names].to_dict()
    broadening = [(curr_df[f'gamma_{ii}'], curr_df[f'sigma_{ii}']) for ii in range(6)]
    shifting = [curr_df[f'eps_{ii}'] for ii in range(6)]
    baseline = sum(([curr_df[f'B_real_{x}'], curr_df[f'B_imag_{x}']] for x in range(4)), [])
    baseline = [bline * 0.0 for bline in baseline]
    phi0 = 0.0
    phi1 = 0.0
    fid, header, concs = synth.syntheticFromBasisFile(
        basis_file,
        ignore=['HG', 'Ala', 'PE'],
        metab_groups='combine_all',
        concentrations=concs,
        baseline=baseline,
        baseline_ppm=(0.2, 4.2),
        broadening=broadening,
        shifting=shifting,
        phi0=phi0,
        phi1=phi1,
        noisecovariance=[[0]],
        bandwidth=1250.0,
        points=512,
        add_default_mm=True)
    all_syn_fids.append(fid/curr_df['fid_scale'])

# Generate the MRSI grid
fullarray = np.zeros(all_input_data[0].data.shape, dtype=complex)
all_indicies = all_input_data[0].get_indicies_in_order()
all_indicies = [ind+(slice(None),) for ind in all_indicies]
for ind, fid in zip(all_indicies, all_syn_fids):
    fullarray[ind] = fid

# Generate NIfTI MRS object and save to file.
print('Saving synthetic data.')
nmrs_noiseless = gen_new_nifti_mrs(fullarray,
                                   1 / header['bandwidth'],
                                   header['centralFrequency'],
                                   affine=output_affine)
nmrs_noiseless.save(output_folder / ('noiseless_synthetic_' + single_time_Str))
