import argparse
from fsl_mrs.utils import mrs_io
import pandas as pd
import numpy as np
from fsl_mrs.core.nifti_mrs import gen_new_nifti_mrs
from pathlib import Path


def main(raw_args=None):
    parser = argparse.ArgumentParser()
    parser.add_argument('outstr', help="Output_str", type=str)
    parser.add_argument('noiseless', help="Path to noiseless data", type=Path)
    parser.add_argument('fit_results', help="Path to fit results dataframe",
                        type=Path)
    parser.add_argument('-n', "--noise_scale",
                        help="Noise scaling factor",
                        type=float,
                        default=1.0)
    parser.add_argument('-r', "--repetitions",
                        help="MC repetitions",
                        type=int,
                        default=10)
    parser.add_argument('-o', "--out_dir",
                        help="Output path",
                        type=Path,
                        default='.')
    args = parser.parse_args(raw_args)

    if not args.out_dir.exists():
        args.out_dir.mkdir(parents=True, exist_ok=True)

    nmrs_noiseless = mrs_io.read_FID(str(args.noiseless))
    df_final = pd.read_pickle(args.fit_results)

    data_shape = nmrs_noiseless.shape
    for ii in range(args.repetitions):
        noisecovariance = (df_final['noise_sd'].median()/np.sqrt(2))**2 \
                          * np.eye(data_shape[3])\
                          * args.noise_scale

        noise = np.random.multivariate_normal(np.zeros((data_shape[3])),
                                              noisecovariance,
                                              data_shape[0:3]) \
            + 1j * np.random.multivariate_normal(np.zeros((data_shape[3])),
                                                 noisecovariance,
                                                 data_shape[0:3])
        noise /= df_final['fid_scale'].median()

        nmrs = gen_new_nifti_mrs(nmrs_noiseless.data + noise,
                                 nmrs_noiseless.dwelltime,
                                 nmrs_noiseless.spectrometer_frequency[0],
                                 affine=nmrs_noiseless.voxToWorldMat)
        out_str = f'{args.outstr}_scale{args.noise_scale:0.3f}__{ii:03.0f}'
        out_str = out_str.replace('.', '_')
        nmrs.save(str(args.out_dir / out_str))


if __name__ == '__main__':
    main()
