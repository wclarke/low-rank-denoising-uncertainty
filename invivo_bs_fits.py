"""
Script to run bootstrap fitting of a subset of the in vivo data.
Uses all the different ST approaches:
   1. Global ST - MP
   2. Local ST - MP

This script implements this stage on the FMRIB cluster and makes calls using the SGE qsub command to spool up multiple
jobs (one for fit operation)
"""

import subprocess
import socket
from pathlib import Path

hostname = socket.gethostname()
if hostname == 'jalapeno.cluster.fmrib.ox.ac.uk':
    on_cluster = True
else:
    on_cluster = False

test_run = False

data_dir = Path('data/in_vivo')
# Only fit first single average
data_names = [f'single_avg_{ii:02.0f}.nii.gz' for ii in range(1,2)]
# data_names.append('combined.nii.gz')

out_base = Path('invivo_bootstrap_fits')
out_base.mkdir(exist_ok=True)

basis_location = 'data/basis_set'
bs_reps = 40

def gen_fit_call(data, wref, mask, output_loc):
    return ['fsl_mrsi',
            '--data', data,
            '--mask', mask,
            '--h2o', wref,
            '--output', output_loc,
            '--config', 'invivo_fit_config.txt']


def fit_job(data, wref, mask, output, jobid=None):
    """Generate a fitting job waiting on completion of a jobid and running on a particular nifti file name"""

    fit_call = gen_fit_call(data, wref, mask, output)

    if on_cluster:
        if jobid:
            subprocess.check_call(['fsl_sub', '-q', 'veryshort.q', '-j', jobid] + fit_call + ['--single_proc', ])
        else:
            subprocess.check_call(['fsl_sub', '-q', 'veryshort.q'] + fit_call + ['--single_proc', ])
    else:
        subprocess.check_call(fit_call)


def denoise_job(denoise_call):
    """Make call for denoising process"""
    if on_cluster:
        return subprocess.check_output(['fsl_sub', '-q', 'veryshort.q'] + denoise_call)\
            .decode('utf-8').rstrip()
    else:
        subprocess.check_call(denoise_call)
        return None


def gen_bsfit_call(data, wref, mask, output_loc):
    return ['python',
            'bootstrap_fit.py',
            data, output_loc, mask,
            '--ref', wref,
            '--fit_config', 'invivo_fit_config.txt',
            '--reps', str(bs_reps)]


def bs_fit_job(data, wref, mask, output, jobid=None):
    """Generate a bs fitting job waiting on completion of a jobid and running on a particular nifti file name"""

    fit_call = gen_bsfit_call(data, wref, mask, output)

    if on_cluster:
        if jobid:
            subprocess.check_call(['fsl_sub', '-q', 'long.q', '-j', jobid] + fit_call)
        else:
            subprocess.check_call(['fsl_sub', '-q', 'long.q'] + fit_call)
    else:
        subprocess.check_call(fit_call)


# loop over the 5 subjects 
for sdx in range(5):
    sub_dir = data_dir / f'hv10{sdx + 1}'
    sub_out = out_base / f'hv10{sdx + 1}'
    sub_out.mkdir(exist_ok=True)
    
    # 1 - Global ST using MP
    out_loc = sub_out / 'st_g'
    out_loc.mkdir(exist_ok=True)
    for name in data_names:
        curr_data = sub_dir / name
        curr_index = name[:-7]
        denoise_out = str(out_loc / f'denoise_{curr_index}.nii.gz')
        fit_out = str(out_loc / f'fit_{curr_index}')
        mask = sub_dir / 'mask.nii.gz'  
        denoise_call_st = ['mrsi_denoise', 'st',
                           '--mask', str(mask),
                           '-mp',
                           str(curr_data),
                           denoise_out,
                           '300', '512']

        jid = denoise_job(denoise_call_st)

        wref = sub_dir / 'wref.nii.gz'
        bs_fit_job(denoise_out, str(wref), str(mask), fit_out, jobid=jid)

        if test_run:
            break

    # 2 - Local ST using MP
    out_loc = sub_out / 'st_l'
    out_loc.mkdir(exist_ok=True)
    for name in data_names:
        curr_data = sub_dir / name
        curr_index = name[:-7]
        denoise_out = str(out_loc / f'denoise_{curr_index}.nii.gz')
        fit_out = str(out_loc / f'fit_{curr_index}')
        mask = sub_dir / 'mask.nii.gz'  
        denoise_call_st = ['mrsi_denoise', 'st',
                           '--mask', str(mask),
                           '-mp',
                           '-p', '3', '3', '1',
                           str(curr_data),
                           denoise_out,
                           '300', '512']

        jid = denoise_job(denoise_call_st)
        
        wref = sub_dir / 'wref.nii.gz'
        bs_fit_job(denoise_out, str(wref), str(mask), fit_out, jobid=jid)

        if test_run:
            break

    # 3 - Control Noisy data fit
    # First generate the data (with variance estimate) by
    # calling the st_denoising without any truncation.
    out_loc = sub_out / 'noisy'
    out_loc.mkdir(exist_ok=True)
    for name in data_names:
        curr_data = sub_dir / name
        curr_index = name[:-7]
        denoise_out = str(out_loc / f'denoise_{curr_index}.nii.gz')
        fit_out = str(out_loc / f'fit_{curr_index}')
        mask = sub_dir / 'mask.nii.gz'  
        denoise_call_st = ['mrsi_denoise', 'st',
                           '--mask', str(mask),
                           str(curr_data),
                           denoise_out,
                           '300', '512']

        jid = denoise_job(denoise_call_st)
        
        wref = sub_dir / 'wref.nii.gz'
        bs_fit_job(denoise_out, str(wref), str(mask), fit_out, jobid=jid)

        if test_run:
            break
    
    if test_run:
            break
