"""
Script to denoise and fit the in vivo data.
Uses all the different approaches:
   1. Global ST - MP
   2. Local ST - MP
   3. LP - Fixed
   4. LORA - MP + fixed
   5. Local LORA - MP + fixed
Fitting using FSL-MRS Newton algorithm. No bootstrap fitting.

This script implements this stage on the FMRIB cluster and makes calls using the SGE qsub command to spool up multiple
jobs (one for each denoise and fit operation)
For each denoising case the script loops through the availible data runs the denoising as a job and spawns a fitting
job which waits on completion of the first.
"""

import subprocess
import socket
from pathlib import Path

hostname = socket.gethostname()
if hostname == 'jalapeno.cluster.fmrib.ox.ac.uk':
    on_cluster = True
else:
    on_cluster = False

test_run = False

data_dir = Path('data/in_vivo')
data_names = [f'single_avg_{ii:02.0f}.nii.gz' for ii in range(1,11)]
data_names.append('combined.nii.gz')

out_base = Path('invivo_fits')
out_base.mkdir(exist_ok=True)

basis_location = 'data/basis_set'


def gen_fit_call(data, wref, mask, output_loc):
    return ['fsl_mrsi',
            '--data', data,
            '--mask', mask,
            '--h2o', wref,
            '--output', output_loc,
            '--config', 'invivo_fit_config.txt']


def fit_job(data, wref, mask, output, jobid=None):
    """Generate a fitting job waiting on completion of a jobid and running on a particular nifti file name"""

    fit_call = gen_fit_call(data, wref, mask, output)

    if on_cluster:
        if jobid:
            subprocess.check_call(['fsl_sub', '-q', 'veryshort.q', '-j', jobid] + fit_call + ['--single_proc', ])
        else:
            subprocess.check_call(['fsl_sub', '-q', 'veryshort.q'] + fit_call + ['--single_proc', ])
    else:
        subprocess.check_call(fit_call)


def denoise_job(denoise_call):
    """Make call for denoising process"""
    if on_cluster:
        return subprocess.check_output(['fsl_sub', '-q', 'veryshort.q'] + denoise_call)\
            .decode('utf-8').rstrip()
    else:
        subprocess.check_call(denoise_call)
        return None


# loop over the 5 subjects 
for sdx in range(5):
    sub_dir = data_dir / f'hv10{sdx + 1}'
    sub_out = out_base / f'hv10{sdx + 1}'
    sub_out.mkdir(exist_ok=True)
    
    # 1 - Global ST using MP
    out_loc = sub_out / 'st_g'
    out_loc.mkdir(exist_ok=True)
    for name in data_names:
        curr_data = sub_dir / name
        curr_index = name[:-7]
        denoise_out = str(out_loc / f'denoise_{curr_index}.nii.gz')
        fit_out = str(out_loc / f'fit_{curr_index}')
        mask = sub_dir / 'mask.nii.gz'  
        denoise_call_st = ['mrsi_denoise', 'st',
                           '--mask', str(mask),
                           '-mp',
                           str(curr_data),
                           denoise_out,
                           '300', '512']

        jid = denoise_job(denoise_call_st)

        wref = sub_dir / 'wref.nii.gz'
        fit_job(denoise_out, str(wref), str(mask), fit_out, jobid=jid)

        if test_run:
            break

    # 2 - Local ST using MP
    out_loc = sub_out / 'st_l'
    out_loc.mkdir(exist_ok=True)
    for name in data_names:
        curr_data = sub_dir / name
        curr_index = name[:-7]
        denoise_out = str(out_loc / f'denoise_{curr_index}.nii.gz')
        fit_out = str(out_loc / f'fit_{curr_index}')
        mask = sub_dir / 'mask.nii.gz'  
        denoise_call_st = ['mrsi_denoise', 'st',
                           '--mask', str(mask),
                           '-mp',
                           '-p', '3', '3', '1',
                           str(curr_data),
                           denoise_out,
                           '300', '512']

        jid = denoise_job(denoise_call_st)
        
        wref = sub_dir / 'wref.nii.gz'
        fit_job(denoise_out, str(wref), str(mask), fit_out, jobid=jid)

        if test_run:
            break

    # 3 - LP - Fixed
    out_loc = sub_out / 'lp'
    out_loc.mkdir(exist_ok=True)
    for name in data_names:
        curr_data = sub_dir / name
        curr_index = name[:-7]
        denoise_out = str(out_loc / f'denoise_{curr_index}.nii.gz')
        fit_out = str(out_loc / f'fit_{curr_index}')
        mask = sub_dir / 'mask.nii.gz'  
        denoise_call_st = ['mrsi_denoise', 'lp',
                           '--mask', str(mask),
                           str(curr_data),
                           denoise_out,
                           '20']

        jid = denoise_job(denoise_call_st)

        wref = sub_dir / 'wref.nii.gz' 
        fit_job(denoise_out, str(wref), str(mask), fit_out, jobid=jid)

        if test_run:
            break

    # 4 - Global LORA using MP + fixed r2
    out_loc = sub_out / 'lo_g'
    out_loc.mkdir(exist_ok=True)
    for name in data_names:
        curr_data = sub_dir / name
        curr_index = name[:-7]
        denoise_out = str(out_loc / f'denoise_{curr_index}.nii.gz')
        fit_out = str(out_loc / f'fit_{curr_index}')
        mask = sub_dir / 'mask.nii.gz'  
        denoise_call_st = ['mrsi_denoise', 'lora',
                           '--mask', str(mask),
                           '-mp',
                           '-n', '300', '512',
                           '-r2', '20',
                           str(curr_data),
                           denoise_out]

        jid = denoise_job(denoise_call_st)

        wref = sub_dir / 'wref.nii.gz'
        fit_job(denoise_out, str(wref), str(mask), fit_out, jobid=jid)

        if test_run:
            break

    # 5 - Local LORA using MP + fixed r2
    out_loc = sub_out / 'lo_l'
    out_loc.mkdir(exist_ok=True)
    for name in data_names:
        curr_data = sub_dir / name
        curr_index = name[:-7]
        denoise_out = str(out_loc / f'denoise_{curr_index}.nii.gz')
        fit_out = str(out_loc / f'fit_{curr_index}')
        mask = sub_dir / 'mask.nii.gz'  
        denoise_call_st = ['mrsi_denoise', 'lora',
                           '--mask', str(mask),
                           '-mp',
                           '-n', '300', '512',
                           '-r2', '20',
                           '-p', '3', '3', '1',
                           str(curr_data),
                           denoise_out]

        jid = denoise_job(denoise_call_st)

        wref = sub_dir / 'wref.nii.gz'
        fit_job(denoise_out, str(wref), str(mask), fit_out, jobid=jid)

        if test_run:
            break

    # 6 - Noisy data fit
    out_loc = sub_out / 'noisy'
    out_loc.mkdir(exist_ok=True)
    for name in data_names:
        curr_data = sub_dir / name
        curr_index = name[:-7]
        fit_out = str(out_loc / f'fit_{curr_index}')
        wref = sub_dir / 'wref.nii.gz'
        mask = sub_dir / 'mask.nii.gz'  
        fit_job(str(curr_data), str(wref), str(mask), fit_out, jobid=None)

        if test_run:
            break
    
    if test_run:
            break
