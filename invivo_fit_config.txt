# Configuration file for the in vivo fits
basis = data/basis_set
baseline_order = 3
add_MM
ignore = [HG, Ala, PE]
TE = 0.0
TR = 15.0
combine = [Cr, PCr]
combine = [PCho, GPC]
combine = [NAA, NAAG]
combine = [Glu, Gln]
combine = [Glc, Tau]
overwrite