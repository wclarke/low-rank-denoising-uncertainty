"""
Script to run fitting of noiseless synthetic data
"""
import subprocess
import socket
from pathlib import Path

hostname = socket.gethostname()
if hostname == 'jalapeno.cluster.fmrib.ox.ac.uk':
    on_cluster = True
else:
    on_cluster = False

test_run = False

data_dir = Path('fit_for_synthetic')
data_name = 'noiseless_synthetic_*.nii.gz'

out_base = Path('noiseless_fit')
out_base.mkdir(exist_ok=True)

basis_location = 'data/basis_set'
wref_location = 'noisy_synthetic_data/wref.nii.gz'
mask_location = 'noisy_synthetic_data/mask.nii.gz'


def gen_fit_call(data, output_loc):
    return ['fsl_mrsi',
            '--data', data,
            '--basis', basis_location,
            '--mask', mask_location,
            '--h2o', wref_location,
            '--output', output_loc,
            '--baseline_order', '-1',
            '--add_MM',
            '--ignore', 'HG',
            '--ignore', 'Ala',
            '--ignore', 'PE',
            '--TE', '0.0',
            '--TR', '15.0',
            '--combine', 'Cr', 'PCr',
            '--combine', 'PCho', 'GPC',
            '--combine', 'NAA', 'NAAG',
            '--combine', 'Glu', 'Gln',
            '--combine', 'Glc', 'Tau',
            '--overwrite']


def fit_job(data, output, jobid=None):
    """Generate a fitting job waiting on completion of a jobid and running on a particular nifti file name"""

    fit_call = gen_fit_call(data, output)

    if on_cluster:
        if jobid:
            subprocess.check_call(['fsl_sub', '-q', 'veryshort.q', '-j', jobid] + fit_call + ['--single_proc', ])
        else:
            subprocess.check_call(['fsl_sub', '-q', 'veryshort.q'] + fit_call + ['--single_proc', ])
    else:
        subprocess.check_call(fit_call)
        

# noiseless data fit
out_loc = out_base
out_loc.mkdir(exist_ok=True)
for nd in data_dir.glob(data_name):
    curr_index = nd.name[:-7]
    fit_out = str(out_loc / f'fit_{curr_index}')
    fit_job(str(nd), fit_out, jobid=None)

    if test_run:
        break
