"""
Script to denoise and fit the different noise instantiations from a single noise level.
Uses all the different approaches:
   1. Global ST - MP
   2. Local ST - MP
   3. LP - Fixed
   4. LORA - MP + fixed
   5. Local LORA - MP + fixed
Fitting using FSL-MRS Newton algorithm. No bootstrap fitting (variance analysis in separate script)

This script implements this stage on the FMRIB cluster and makes calls using the SGE qsub command to spool up multiple
jobs (one for each denoise and fit operation)
For each denoising case the script loops through the availible data runs the denoising as a job and spawns a fitting
job which waits on completion of the first.
"""

import subprocess
import socket
from pathlib import Path

hostname = socket.gethostname()
if hostname == 'jalapeno.cluster.fmrib.ox.ac.uk':
    on_cluster = True
else:
    on_cluster = False

test_run = False

noisy_data_dir = Path('noisy_synthetic_data/noise_5')
noisy_data_name = 'noisy_synthetic_scale5_000__*.nii.gz'

out_base = Path('synthetic_fits_1')
out_base.mkdir(exist_ok=True)

basis_location = 'data/basis_set'
wref_location = 'noisy_synthetic_data/wref.nii.gz'
mask_location = 'noisy_synthetic_data/mask.nii.gz'


def gen_fit_call(data, output_loc):
    return ['fsl_mrsi',
            '--data', data,
            '--basis', basis_location,
            '--mask', mask_location,
            '--h2o', wref_location,
            '--output', output_loc,
            '--baseline_order', '-1',
            '--add_MM',
            '--ignore', 'HG',
            '--ignore', 'Ala',
            '--ignore', 'PE',
            '--TE', '0.0',
            '--TR', '15.0',
            '--combine', 'Cr', 'PCr',
            '--combine', 'PCho', 'GPC',
            '--combine', 'NAA', 'NAAG',
            '--combine', 'Glu', 'Gln',
            '--combine', 'Glc', 'Tau',
            '--overwrite']


def fit_job(data, output, jobid=None):
    """Generate a fitting job waiting on completion of a jobid and running on a particular nifti file name"""

    fit_call = gen_fit_call(data, output)

    if on_cluster:
        if jobid:
            subprocess.check_call(['fsl_sub', '-q', 'veryshort.q', '-j', jobid] + fit_call + ['--single_proc', ])
        else:
            subprocess.check_call(['fsl_sub', '-q', 'veryshort.q'] + fit_call + ['--single_proc', ])
    else:
        subprocess.check_call(fit_call)


def denoise_job(denoise_call):
    """Make call for denoising process"""
    if on_cluster:
        return subprocess.check_output(['fsl_sub', '-q', 'veryshort.q'] + denoise_call)\
            .decode('utf-8').rstrip()
    else:
        subprocess.check_call(denoise_call)
        return None


# 1 - Global ST using MP
out_loc = out_base / 'st_g'
out_loc.mkdir(exist_ok=True)
for nd in noisy_data_dir.glob(noisy_data_name):
    curr_index = nd.name[-10:-7]
    denoise_out = str(out_loc / f'denoise_{curr_index}.nii.gz')
    fit_out = str(out_loc / f'fit_{curr_index}')
    denoise_call_st = ['mrsi_denoise', 'st',
                       '--mask', mask_location,
                       '-mp',
                       str(nd),
                       denoise_out,
                       '300', '512']

    jid = denoise_job(denoise_call_st)

    fit_job(denoise_out, fit_out, jobid=jid)

    if test_run:
        break

# 2 - Local ST using MP
out_loc = out_base / 'st_l'
out_loc.mkdir(exist_ok=True)
for nd in noisy_data_dir.glob(noisy_data_name):
    curr_index = nd.name[-10:-7]
    denoise_out = str(out_loc / f'denoise_{curr_index}.nii.gz')
    fit_out = str(out_loc / f'fit_{curr_index}')
    denoise_call_st = ['mrsi_denoise', 'st',
                       '--mask', mask_location,
                       '-mp',
                       '-p', '3', '3', '1',
                       str(nd),
                       denoise_out,
                       '300', '512']

    jid = denoise_job(denoise_call_st)

    fit_job(denoise_out, fit_out, jobid=jid)

    if test_run:
        break

# 3 - LP - Fixed
out_loc = out_base / 'lp'
out_loc.mkdir(exist_ok=True)
for nd in noisy_data_dir.glob(noisy_data_name):
    curr_index = nd.name[-10:-7]
    denoise_out = str(out_loc / f'denoise_{curr_index}.nii.gz')
    fit_out = str(out_loc / f'fit_{curr_index}')
    denoise_call_st = ['mrsi_denoise', 'lp',
                       '--mask', mask_location,
                       str(nd),
                       denoise_out,
                       '20']

    jid = denoise_job(denoise_call_st)

    fit_job(denoise_out, fit_out, jobid=jid)

    if test_run:
        break

# 4 - Global LORA using MP + fixed r2
out_loc = out_base / 'lo_g'
out_loc.mkdir(exist_ok=True)
for nd in noisy_data_dir.glob(noisy_data_name):
    curr_index = nd.name[-10:-7]
    denoise_out = str(out_loc / f'denoise_{curr_index}.nii.gz')
    fit_out = str(out_loc / f'fit_{curr_index}')
    denoise_call_st = ['mrsi_denoise', 'lora',
                       '--mask', mask_location,
                       '-mp',
                       '-n', '300', '512',
                       '-r2', '20',
                       str(nd),
                       denoise_out]

    jid = denoise_job(denoise_call_st)

    fit_job(denoise_out, fit_out, jobid=jid)

    if test_run:
        break

# 5 - Local LORA using MP + fixed r2
out_loc = out_base / 'lo_l'
out_loc.mkdir(exist_ok=True)
for nd in noisy_data_dir.glob(noisy_data_name):
    curr_index = nd.name[-10:-7]
    denoise_out = str(out_loc / f'denoise_{curr_index}.nii.gz')
    fit_out = str(out_loc / f'fit_{curr_index}')
    denoise_call_st = ['mrsi_denoise', 'lora',
                       '--mask', mask_location,
                       '-mp',
                       '-n', '300', '512',
                       '-r2', '20',
                       '-p', '3', '3', '1',
                       str(nd),
                       denoise_out]

    jid = denoise_job(denoise_call_st)

    fit_job(denoise_out, fit_out, jobid=jid)

    if test_run:
        break

# 6 - Noisy data fit
out_loc = out_base / 'noisy'
out_loc.mkdir(exist_ok=True)
for nd in noisy_data_dir.glob(noisy_data_name):
    curr_index = nd.name[-10:-7]
    fit_out = str(out_loc / f'fit_{curr_index}')
    fit_job(str(nd), fit_out, jobid=None)

    if test_run:
        break
