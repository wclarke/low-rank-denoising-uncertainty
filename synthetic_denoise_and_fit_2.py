"""
Script to denoise and fit the different noise instantiations from multiple noise levels.
Uses restricted list of approaches:
   1. Global ST - MP
   2. Local ST - MP
   3. Noisy data fit - no BS
Fitting using FSL-MRS Newton algorithm with bootstrap fitting on a subset of the data.

This script implements this stage on the FMRIB cluster and makes calls using the SGE qsub command to spool up multiple
jobs (one for each denoise and fit operation)
For each denoising case the script loops through the availible data runs the denoising as a job and spawns a fitting
job which waits on completion of the first.
"""

import subprocess
import socket
from pathlib import Path

hostname = socket.gethostname()
if hostname == 'jalapeno.cluster.fmrib.ox.ac.uk':
    on_cluster = True
else:
    on_cluster = False

test_run = False
bs_subset_size = 5
bs_reps = 40
noisy_data_dir = Path('noisy_synthetic_data')


out_base = Path('synthetic_fits_2')
out_base.mkdir(exist_ok=True)

mask_location = 'noisy_synthetic_data/mask.nii.gz'


def gen_fit_call(data, output_loc):
    return ['fsl_mrsi',
            '--data', data,
            '--output', output_loc,
            '--config', 'synthetic_fit_config.txt']


def fit_job(data, output, jobid=None):
    """Generate a fitting job waiting on completion of a jobid and running on a particular nifti file name"""

    fit_call = gen_fit_call(data, output)

    if on_cluster:
        if jobid:
            subprocess.check_call(['fsl_sub', '-q', 'veryshort.q', '-j', jobid] + fit_call + ['--single_proc', ])
        else:
            subprocess.check_call(['fsl_sub', '-q', 'veryshort.q'] + fit_call + ['--single_proc', ])
    else:
        subprocess.check_call(fit_call)


def gen_bsfit_call(data, output_loc):
    return ['python',
            'bootstrap_fit.py',
            data, output_loc, mask_location,
            '--fit_config', 'synthetic_fit_config.txt',
            '--reps', str(bs_reps)]


def bs_fit_job(data, output, jobid=None):
    """Generate a bs fitting job waiting on completion of a jobid and running on a particular nifti file name"""

    fit_call = gen_bsfit_call(data, output)

    if on_cluster:
        if jobid:
            subprocess.check_call(['fsl_sub', '-q', 'long.q', '-j', jobid] + fit_call)
        else:
            subprocess.check_call(['fsl_sub', '-q', 'long.q'] + fit_call)
    else:
        subprocess.check_call(fit_call)


def denoise_job(denoise_call):
    """Make call for denoising process"""
    if on_cluster:
        return subprocess.check_output(['fsl_sub', '-q', 'veryshort.q'] + denoise_call)\
            .decode('utf-8').rstrip()
    else:
        subprocess.check_call(denoise_call)
        return None


# Main fit across noise levels
# Loop over noise levels
for noise_lvl_dir in noisy_data_dir.glob('noise_*'):

    # 1 - Global ST using MP
    out_loc = out_base / noise_lvl_dir.name / 'st_g'
    out_loc.mkdir(exist_ok=True, parents=True)
    for ndx, nd in enumerate(noise_lvl_dir.glob('*.nii.gz')):
        curr_index = nd.name[-10:-7]
        denoise_out = str(out_loc / f'denoise_{curr_index}.nii.gz')
        fit_out = str(out_loc / f'fit_{curr_index}')
        denoise_call_st = ['mrsi_denoise', 'st',
                           '--mask', mask_location,
                           '-mp',
                           str(nd),
                           denoise_out,
                           '300', '512']

        jid = denoise_job(denoise_call_st)

        fit_job(denoise_out, fit_out, jobid=jid)

        if ndx < bs_subset_size:
            bs_fit_out = str(out_loc / f'bs_fit_{curr_index}')
            bs_fit_job(denoise_out, bs_fit_out, jobid=jid)
        if test_run:
            break

    # 2 - Local ST using MP
    out_loc = out_base / noise_lvl_dir.name / 'st_l'
    out_loc.mkdir(exist_ok=True)
    for ndx, nd in enumerate(noise_lvl_dir.glob('*.nii.gz')):
        curr_index = nd.name[-10:-7]
        denoise_out = str(out_loc / f'denoise_{curr_index}.nii.gz')
        fit_out = str(out_loc / f'fit_{curr_index}')
        denoise_call_st = ['mrsi_denoise', 'st',
                           '--mask', mask_location,
                           '-mp',
                           '-p', '3', '3', '1',
                           str(nd),
                           denoise_out,
                           '300', '512']

        jid = denoise_job(denoise_call_st)

        fit_job(denoise_out, fit_out, jobid=jid)

        if ndx < bs_subset_size:
            bs_fit_out = str(out_loc / f'bs_fit_{curr_index}')
            bs_fit_job(denoise_out, bs_fit_out, jobid=jid)

        if test_run:
            break

    # 3 - Noisy data fit
    out_loc = out_base / noise_lvl_dir.name / 'noisy'
    out_loc.mkdir(exist_ok=True)
    for ndx, nd in enumerate(noise_lvl_dir.glob('*.nii.gz')):
        curr_index = nd.name[-10:-7]
        fit_out = str(out_loc / f'fit_{curr_index}')
        fit_job(str(nd), fit_out, jobid=None)

        if test_run:
            break
